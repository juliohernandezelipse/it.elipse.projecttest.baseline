package it.elipse.projectTest.baseline.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginAccessController {
	
	@RequestMapping(value="/loginAccess", method = RequestMethod.POST)
    public String getLoginAccess(@RequestParam("username") String username, @RequestParam("password") String pwd){
		return "LoginAccess";
    }

}
