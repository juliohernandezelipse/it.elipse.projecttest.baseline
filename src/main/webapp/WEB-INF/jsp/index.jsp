<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>


<html>

<head>
	<title>Index</title>
		<!-- Reference Bootstrap files -->
	<link rel="stylesheet"
		 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="text-center">
		<h1>Index Page</h1>
		<br><br>
		
		<security:authorize access="!isAuthenticated()">
		 	 <!-- Add Login Button -->
			<form:form action="${pageContext.request.contextPath}/login" method="GET">
				<button type="submit" class="btn btn-success">Log in</button>						
			</form:form>
			<br>
			
			<!-- Add Signup Button -->
			<form:form id="signupButton_index" action="${pageContext.request.contextPath}/signup" method="GET">
				<button type="submit" class="btn btn-primary">Sign up</button>						
			</form:form>
		</security:authorize>
		
		<security:authorize access="isAuthenticated()">
		 	<!-- Add Logout Button -->
			<div class="text-center">
				<p>Logged in:</p>
				<security:authentication property="principal.username" />
				<br><br>
				<form:form action="${pageContext.request.contextPath}/frontPage" method="GET">			
					<button type="submit" class="btn btn-info">Front page</button>			
				</form:form>
				<br><br>
				<form:form action="${pageContext.request.contextPath}/logout" method="POST">			
					<button type="submit" class="btn btn-danger">Log out</button>			
				</form:form>
			</div>
		</security:authorize>
		
		
	</div>
</body>

</html>