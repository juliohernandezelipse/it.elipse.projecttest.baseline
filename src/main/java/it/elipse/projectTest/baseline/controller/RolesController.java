package it.elipse.projectTest.baseline.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RolesController {		
	
	@RequestMapping(value="/admin", method = RequestMethod.GET)
    public String getAdminPage(){
        return "admin-role";
    }
	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
    public String getManagerPage(){
        return "manager-role";
    }
	
	@RequestMapping(value="/user", method = RequestMethod.GET)
    public String getUserPage(){
        return "user-role";
    }
	
}
