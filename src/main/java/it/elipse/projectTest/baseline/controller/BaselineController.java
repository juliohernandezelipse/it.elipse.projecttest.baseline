package it.elipse.projectTest.baseline.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BaselineController {
	
	@RequestMapping(value="/", method = RequestMethod.GET)
    public String getIndexPage(){
        return "index";
    }
	
	
	/*@RequestMapping(value="/frontPage", method = RequestMethod.GET)
    public String getFrontPage(){
        return "frontPage";
    }*/
	
	@RequestMapping(value="/access-denied", method = RequestMethod.GET)
    public String getAccessDeniedPage(){
        return "access-denied";
    }
	
}
