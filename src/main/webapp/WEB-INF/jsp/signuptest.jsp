<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html lang="en">

<head>
	
	<title>Sign up Page</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<!-- Reference Bootstrap files -->
	<link rel="stylesheet"
		 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

	<div>
		
		<div id="loginbox" style="margin-top: 50px;"
			class="mainbox col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-2">
			
			<div class="panel panel-info">

				<div class="panel-heading">
					<div class="panel-title">Sign up</div>
				</div>

				<div style="padding-top: 30px" class="panel-body">

					<!-- Sign up Form -->
					<form:form id="sampleform" class="form-horizontal">

					    <!-- Place for messages: error, alert etc ... 
					    <div class="form-group">
					        <div class="col-xs-15">
					            <div>
									
									<c:if test="${param.error != null}">
										<div class="alert alert-danger col-xs-offset-1 col-xs-10">
											Invalid username and password.
										</div>
									</c:if>
									
									<div class="alert alert-success col-xs-offset-1 col-xs-10">
										You have been logged out.
									</div>
								    

					            </div>
					        </div>
					    </div>-->

						<!-- User name -->
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
							
							<input type="text" name="email" placeholder="Email" class="form-control">
						</div>
						
						<!-- First name -->
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
							
							<input type="text" name="firstName" placeholder="First Name" class="form-control">
						</div>
						
						<!-- Last name -->
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
							
							<input type="text" name="lastName" placeholder="Last Name" class="form-control">
						</div>

						<!-- Password -->
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span> 
							
							<input type="password" name="password" placeholder="Password" class="form-control" >
						</div>
						
						<!-- Test Values -->
						<input type="hidden" name="enable" value="1" >
						<input type="hidden" name="createDate" value=null >
						<input type="hidden" name="createUser" value="email" >
						<input type="hidden" name="updateDate" value=null >
						<input type="hidden" name="updateUser" value="email" >
						<input type="hidden" name="lastLogin" value=null >

						<!-- Sign up/Submit Button -->
						<div class="text-center" class="form-group">						
								<button type="submit" class="btn btn-primary" id="btnSubmit">Sign Up</button>
						</div>

					</form:form>
					
					<!-- Add a link to point to back-->
					<div class="text-left">
						<a href="${pageContext.request.contextPath}/">Go back</a>
					</div>

				</div>

			</div>

		</div>

	</div>
	
	<script type="text/javascript">
		
		function buildJsonFormData(form){
			const jsonFormData={};
			for(const pair of new FormData(form)){
				jsonFormData[pair[0]]=pair[1];
			}
			return jsonFormData;
		}

		async function performPostHttpRequest(fetchLink,headers,body){
			if (!fetchLink||!headers||!body) {
				throw new error("One or more POST request parameters was not passed");
			}
			try{
				const rawResponse=await fetch(fetchLink,{
					method:"POST",
					headers: headers,
					body: JSON.stringify(body)
				});
				const content=await rawResponse.json();
			}
			catch(err){
				alert("Error at fetch")
				throw err;
			}
		}

		async function submitForm(e,form){
			e.preventDefault();
			const btnSubmit=document.getElementById("btnSubmit");
			btnSubmit.disable=true;
			setTimeout(()=>btnSubmit.disable=false,2000);
			const jsonFormData=buildJsonFormData(form);
			const headers=buildHeaders();
			const response=await fetchService.performPostHttpRequest('http://localhost:8080/users',headers,jsonFormData);
			console.alert(response);
		}

		const sampleForm=document.querySelector("#sampleForm");
		if(sampleForm){
			sampleForm.addEventListener("submit",function(e){
				submitForm(e,this);
			});
		}

		
	</script>

</body>
</html>