package it.elipse.projectTest.baseline.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import it.elipse.projectTest.baseline.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
