<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
	<head>
		<title>User Page</title>
		<!-- Reference Bootstrap files -->
		<link rel="stylesheet"
			 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		
		<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<hr>
			<blockquote class="blockquote text-center">
				<h1>Front Page</h1>
				<security:authorize access="hasRole('ADMIN')">
						<h1>Admin Section</h1>
						<br><br>
						<p>
							<a href="${pageContext.request.contextPath}/admin">Admin Functionalities</a>
						</p>
				</security:authorize>
				
				<security:authorize access="hasRole('MANAGER')">
						<h1>Manager Section</h1>
						<br><br>
						<p>
							<a href="${pageContext.request.contextPath}/manager">Manager Functionalities</a>
						</p>
				</security:authorize>
				<security:authorize access="hasRole('USER')">
						<h1>User Section</h1>
						<br><br>
						<p>
							<a href="${pageContext.request.contextPath}/user">User Functionalities</a>
						</p>
				</security:authorize>
			</blockquote>
		<hr>
	
		<p class=" text-center">
			User: <security:authentication property="principal.username" />
			<br><br>
			Role(s): <security:authentication property="principal.authorities"/>
			<br><br>
			
		</p>
		
		
		
			<!-- Add Logout Button -->
		<div class="text-center">
			<form:form action="${pageContext.request.contextPath}/logout" method="POST">			
				<button type="submit" class="btn btn-danger">Log out</button>			
			</form:form>
		</div>
	</body>
</html>